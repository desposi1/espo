package coffee.espo.web.api;

public enum PermissionValueType {

	DENIED,
	ALLOWED,
	GRANT;
}
