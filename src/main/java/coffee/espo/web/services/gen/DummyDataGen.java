package coffee.espo.web.services.gen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

@Service
public class DummyDataGen implements CommandLineRunner {

	@Autowired private UserRoleGen userRoleGen;
	@Autowired private PermissionGen permDefGen;
	
	@Override
	public void run(String... args) throws Exception {
		userRoleGen.gen();
		permDefGen.gen();
	}
}
