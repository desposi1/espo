package coffee.espo.web.services.gen;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gwt.thirdparty.guava.common.collect.Lists;

import coffee.espo.web.domain.UserRole;
import coffee.espo.web.repositories.UserRoleRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserRoleGen {

	public static final String ADMIN = "Admin";
	public static final String USER = "User";
	public static final String ANON = "Anonymous";
	public static final String MOD = "Moderator";
	
	public static final List<String> DEFAULT_ROLES = Collections.unmodifiableList(
			Lists.newArrayList(ADMIN, USER, ANON, MOD));
	
	@Autowired private UserRoleRepository repo;
	
	public void gen() {
		if (repo.count() == 0) {
			DEFAULT_ROLES.stream()
					.forEach(name -> repo.save(UserRole.builder()
							.name(name)
							.description(name + " role.")
							.build()));
			log.info("Generated {} user roles", repo.count());
		} else {
			log.info("Skipping user role gen. {} pre-existing roles", repo.count());
		}
	}
}
