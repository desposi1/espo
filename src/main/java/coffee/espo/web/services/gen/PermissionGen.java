package coffee.espo.web.services.gen;

import static coffee.espo.web.api.PermissionValueType.ALLOWED;
import static coffee.espo.web.api.PermissionValueType.DENIED;
import static coffee.espo.web.api.PermissionValueType.GRANT;
import static coffee.espo.web.services.gen.UserRoleGen.ADMIN;
import static coffee.espo.web.services.gen.UserRoleGen.ANON;
import static coffee.espo.web.services.gen.UserRoleGen.MOD;
import static coffee.espo.web.services.gen.UserRoleGen.USER;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.gwt.thirdparty.guava.common.collect.Maps;

import coffee.espo.web.api.PermissionValueType;
import coffee.espo.web.domain.Permission;
import coffee.espo.web.domain.PermissionDefinition;
import coffee.espo.web.domain.PermissionGroup;
import coffee.espo.web.repositories.PermissionDefinitionRepository;
import coffee.espo.web.repositories.PermissionGroupRepository;
import coffee.espo.web.repositories.PermissionRepository;
import coffee.espo.web.repositories.UserRoleRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PermissionGen {

	public static final String LOGIN = "canLogin";
	public static final String MAN_ROLES = "canManageRoles";
	public static final String MAN_PERMS = "canManagePermissions";
	public static final String BAN = "canBanUser";
	
	private static final Map<String, Map<String, PermissionValueType>> DEFAULT_PERM_MAP;
	
	static {
		Map<String, Map<String, PermissionValueType>> temp = Maps.newHashMap();
		HashMap<String, PermissionValueType> valMap = Maps.newHashMap();
		valMap.put(LOGIN, GRANT);
		valMap.put(MAN_ROLES, GRANT);
		valMap.put(MAN_PERMS, GRANT);
		valMap.put(BAN, GRANT);
		temp.put(ADMIN, valMap);
		
		valMap = Maps.newHashMap();
		valMap.put(LOGIN, ALLOWED);
		valMap.put(MAN_ROLES, DENIED);
		valMap.put(MAN_PERMS, DENIED);
		valMap.put(BAN, DENIED);
		temp.put(USER, valMap);
		
		valMap = Maps.newHashMap();
		valMap.put(LOGIN, DENIED);
		valMap.put(MAN_ROLES, DENIED);
		valMap.put(MAN_PERMS, DENIED);
		valMap.put(BAN, DENIED);
		temp.put(ANON, valMap);
		
		valMap = Maps.newHashMap();
		valMap.put(LOGIN, ALLOWED);
		valMap.put(MAN_ROLES, DENIED);
		valMap.put(MAN_PERMS, DENIED);
		valMap.put(BAN, ALLOWED);
		temp.put(MOD, valMap);
		
		DEFAULT_PERM_MAP = Collections.unmodifiableMap(temp);
	}
	
	@Autowired private PermissionDefinitionRepository defRepo;
	@Autowired private UserRoleRepository userRoleRepo;
	@Autowired private PermissionRepository permRepo;
	@Autowired private PermissionGroupRepository groupRepo;
	
	public void gen() {
		if (defRepo.count() == 0) {
			Stream.of(LOGIN, MAN_ROLES, MAN_PERMS, BAN)
					.forEach(name -> defRepo.save(PermissionDefinition.builder()
							.name(name)
							.description(name + " permission.")
							.build())
					);
			log.info("Generated {} user permission definitions", defRepo.count());
			
			userRoleRepo.findAll().forEach(role -> {

				PermissionGroup group = groupRepo.save(PermissionGroup.builder()
						.permissions(Lists.newArrayList())
						.role(role)
						.build());
				
				List<Permission> perms = defRepo.findAll().stream()
						.map(def -> Permission.builder()
								.definition(def)
								.group(group)
								.value(DEFAULT_PERM_MAP.get(role.getName()).get(def.getName()))
								.build())
						.collect(Collectors.toList());
				perms = permRepo.save(perms);
				group.setPermissions(perms);
				groupRepo.save(group);
			});
			
			log.info("Generated {} user permissions", permRepo.count());
			log.info("Generated {} user permission groups", groupRepo.count());
		} else {
			log.info("Skipping user permission definition gen. {} pre-existing permission definitions", defRepo.count());
		}
	}
}
