package coffee.espo.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import coffee.espo.web.domain.PermissionDefinition;

public interface PermissionDefinitionRepository extends JpaRepository<PermissionDefinition, Long> {

}
