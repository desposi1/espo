package coffee.espo.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import coffee.espo.web.domain.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

}
