package coffee.espo.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import coffee.espo.web.domain.Permission;

public interface PermissionRepository extends JpaRepository<Permission, Long> {

}
