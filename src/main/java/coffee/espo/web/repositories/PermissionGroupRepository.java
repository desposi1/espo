package coffee.espo.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import coffee.espo.web.domain.PermissionGroup;

public interface PermissionGroupRepository extends JpaRepository<PermissionGroup, Long> {

}
