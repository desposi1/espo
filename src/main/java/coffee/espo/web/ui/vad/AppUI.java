package coffee.espo.web.ui.vad;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

import coffee.espo.core.utils.CastableUtil;
import coffee.espo.web.core.ui.vad.EspoUI;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@SpringUI
@Theme(ValoTheme.THEME_NAME)
@EqualsAndHashCode(callSuper=false)
public class AppUI extends EspoUI {

	private static final long serialVersionUID = -2012980458310366634L;

	@Getter
	@Autowired private ApplicationContext context;
	
	@Override
	protected void init(VaadinRequest request) {
		super.init(request);
	}
	
	public static AppUI getCurrent() {
		return CastableUtil.as(UI.getCurrent(), AppUI.class);
	}
}
