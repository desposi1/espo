package coffee.espo.web.ui.vad.comps;

import org.springframework.stereotype.Component;

import com.vaadin.ui.Alignment;

import coffee.espo.web.core.ui.vad.comps.ELabel;
import coffee.espo.web.core.ui.vad.comps.ERows;
import coffee.espo.web.core.ui.vad.comps.IHeader;

@Component
public class AppHeader extends ERows implements IHeader {

	private static final long serialVersionUID = -6199791991721021566L;

	public AppHeader() {
		super();
		
		setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
		addComponent(new ELabel("This is the header"));
	}
}
