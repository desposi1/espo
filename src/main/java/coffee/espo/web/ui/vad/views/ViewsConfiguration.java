package coffee.espo.web.ui.vad.views;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.vaadin.spring.annotation.VaadinSessionScope;

import coffee.espo.web.core.ui.vad.views.IErrorView;

@Configuration
public class ViewsConfiguration {
	
	@Bean
	@VaadinSessionScope
	public IErrorView errorView() {
		return new ErrorView();
	}
}
