package coffee.espo.web.ui.vad.views;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import coffee.espo.web.core.ui.vad.comps.ELabel;
import coffee.espo.web.core.ui.vad.views.IErrorView;
import coffee.espo.web.core.ui.vad.views.WebPageView;
import coffee.espo.web.core.ui.vad.views.utils.RegexEnterHandler;
import coffee.espo.web.core.ui.vad.views.utils.SubRoutingView;

@SpringView(name = ErrorView.VIEW_NAME)
public class ErrorView extends WebPageView implements SubRoutingView, IErrorView {

	public static final String VIEW_NAME = "error";
	
	private static final long serialVersionUID = 1L;
	private static final RegexEnterHandler<ErrorView> routeHandler;

	static {
		routeHandler = new RegexEnterHandler<>();
		routeHandler.register(linkMessage(".*"), (me, e) -> {
			List<String> params = me.getPathParams(e.getParameters());
			try {
				String message = URLDecoder.decode(params.get(1), "UTF-8");
				me.enterMessage(message);
				return;
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			me.enterDashbaord();
		});
	}
	
	public ErrorView() {
		super();
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		routeHandler.enter(this, event);
	}
	
	@Override
	public void enterDashbaord() {
		enterMessage("What did you do?! Did you break it?! I hope you can pay for this!!");
	}
	
	public void enterMessage(String message) {
		ELabel errorLabel = new ELabel(message); // TODO: add failure class
		replaceContent(errorLabel);
		setComponentAlignment(errorLabel, Alignment.MIDDLE_CENTER);
		Notification.show("Opps, something went wrong.", Type.WARNING_MESSAGE);
	}
	
	@Override
	public String link() {
		return linkBase();
	}
	
	public static String linkBase() {
		return VIEW_NAME;
	}
	
	public static String linkNotFound() {
		return linkMessage("The requested resource was not found.");
	}
	
	public static String linkMessage(String message) {
		try {
			return linkBase() + "/m/" + URLEncoder.encode(message, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return linkBase();
	}
}
