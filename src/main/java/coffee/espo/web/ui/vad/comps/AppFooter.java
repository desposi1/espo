package coffee.espo.web.ui.vad.comps;

import org.springframework.stereotype.Component;

import com.vaadin.ui.Alignment;

import coffee.espo.web.core.ui.vad.comps.ELabel;
import coffee.espo.web.core.ui.vad.comps.ERows;
import coffee.espo.web.core.ui.vad.comps.IFooter;

@Component
public class AppFooter extends ERows implements IFooter {

	private static final long serialVersionUID = 8349980963437446919L;

	public AppFooter() {
		super();
		
		setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
		addComponent(new ELabel("This is the footer"));
	}
}
