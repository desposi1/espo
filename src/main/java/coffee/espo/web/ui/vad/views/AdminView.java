package coffee.espo.web.ui.vad.views;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;

import coffee.espo.web.core.ui.vad.comps.ELabel;
import coffee.espo.web.core.ui.vad.views.WebPageView;
import coffee.espo.web.core.ui.vad.views.utils.RegexEnterHandler;
import coffee.espo.web.core.ui.vad.views.utils.SubRoutingView;
import coffee.espo.web.ui.vad.comps.admin.AdminContainer;
import coffee.espo.web.ui.vad.comps.admin.NamedObjectManagerContainer;

@SpringView(name = AdminView.VIEW_NAME)
public class AdminView extends WebPageView implements SubRoutingView {
	
	public static final String VIEW_NAME = "admin";
	
	private static final long serialVersionUID = -3706230941960373207L;
	private static final RegexEnterHandler<AdminView> routeHandler;
	
	static {
		routeHandler = new RegexEnterHandler<>();
		routeHandler.register(linkRoles(), (me, e) -> {
			me.enterRoles();
		});
		routeHandler.register(linkPermissionDefinitions(), (me, e) -> {
			me.enterDefinitions();
		});
	}

	@Override
	public void enter(ViewChangeEvent event) {
		routeHandler.enter(this, event);
	}

	@Override
	public void enterDashbaord() {
		replaceContent(new ELabel("Admin"));
	}
	
	protected void enterRoles() {
		replaceContent(new AdminContainer(new NamedObjectManagerContainer.RolePresenter().get()));
	}
	
	protected void enterDefinitions() {
		replaceContent(new AdminContainer(new NamedObjectManagerContainer.PermissionDefinitionPresenter().get()));
	}

	@Override
	public String link() {
		return linkBase();
	}

	public static String linkBase() {
		return VIEW_NAME;
	}
	
	public static String linkRoles() {
		return linkBase() + "/roles";
	}
	
	public static String linkPermissionDefinitions() {
		return linkBase() + "/permissions";
	}
	
	public static String linkPermissionGroups() {
		return linkBase() + "/permission/groups";
	}
}
