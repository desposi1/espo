package coffee.espo.web.ui.vad.views;

import java.util.List;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;

import coffee.espo.web.core.ui.vad.comps.ELabel;
import coffee.espo.web.core.ui.vad.views.WebPageView;
import coffee.espo.web.core.ui.vad.views.utils.RegexEnterHandler;
import coffee.espo.web.core.ui.vad.views.utils.SubRoutingView;

@SpringView(name = HomeView.VIEW_NAME)
public class HomeView extends WebPageView implements SubRoutingView {
	
	public static final String VIEW_NAME = "";
	
	private static final long serialVersionUID = -3706230941960373207L;
	private static final RegexEnterHandler<HomeView> routeHandler;
	
	static {
		routeHandler = new RegexEnterHandler<>();
		routeHandler.register(linkHello("[a-zA-Z]+"), (me, e) -> {
			List<String> params = me.getPathParams(e.getParameters());
			me.enterHello(params.get(1));
		});
	}

	@Override
	public void enter(ViewChangeEvent event) {
		routeHandler.enter(this, event);
	}

	@Override
	public void enterDashbaord() {
		replaceContent(new ELabel("This is the default home view"));
	}
	
	protected void enterHello(String name) {
		replaceContent(new ELabel("Hello, " + name + "!"));
	}

	@Override
	public String link() {
		return linkBase();
	}

	public static String linkBase() {
		return VIEW_NAME;
	}
	
	public static String linkHello(String name) {
		return linkBase() + "/hello/" + name;
	}
}
