package coffee.espo.web.ui.vad.comps.admin;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import com.vaadin.data.Property;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.ui.Button;

import coffee.espo.web.core.event.session.EventSubscriptionManager;
import coffee.espo.web.core.ui.vad.comps.EButton;
import coffee.espo.web.core.ui.vad.comps.EColumns;
import coffee.espo.web.core.ui.vad.comps.EComponent;
import coffee.espo.web.core.ui.vad.comps.ELabel;
import coffee.espo.web.core.ui.vad.comps.ENotification;
import coffee.espo.web.core.ui.vad.comps.ERows;
import coffee.espo.web.core.ui.vad.comps.ETextField;
import coffee.espo.web.core.ui.vad.comps.IComponentPresenter;
import coffee.espo.web.domain.PermissionDefinition;
import coffee.espo.web.domain.UserRole;
import coffee.espo.web.repositories.PermissionDefinitionRepository;
import coffee.espo.web.repositories.UserRoleRepository;
import coffee.espo.web.ui.vad.AppUI;
import lombok.Builder;
import lombok.Data;

public class NamedObjectManagerContainer extends ERows {

	private static final long serialVersionUID = 1L;
	
	private final EventSubscriptionManager<RowContext> saveSubs;
	private final EventSubscriptionManager<RowContext> deleteSubs;
	
	public NamedObjectManagerContainer(List<RowContext> contextList, String typeName) {
		super(true);

		saveSubs = new EventSubscriptionManager<>();
		deleteSubs = new EventSubscriptionManager<>();
		
		contextList.forEach(ctx -> addComponent(getRow(ctx)));
		
		addComponent(new EButton("Add New " + typeName, e -> {
			EComponent row = getRow(RowContext.builder()
					.desc(new ObjectProperty<String>("New " + typeName + " description."))
					.name(new ObjectProperty<String>("New " + typeName))
					.build());
			addComponent(row, getComponentCount() - 1);
		}));
	}
	
	protected EComponent getRow(RowContext ctx) {
		ETextField nameField = new ETextField();
		nameField.setPropertyDataSource(ctx.getName());
		nameField.setWidthFull();
		ETextField descField = new ETextField();
		descField.setPropertyDataSource(ctx.getDesc());
		descField.setWidthFull();
		EColumns row = new EColumns(
				new ELabel(ctx.getId() + ""),
				nameField,
				descField,
				new Button("Save", e -> saveSubs.trigger(ctx)),
				new Button("Delete", e -> deleteSubs.trigger(ctx)));
		row.setWidthFull();
		row.setExpandRatio(nameField, 1f);
		row.setExpandRatio(descField, 3f);
		return row;
	}
	
	public void onSave(Consumer<RowContext> c) {
		saveSubs.register(c);
	}
	
	public void onDelete(Consumer<RowContext> c) {
		deleteSubs.register(c);
	}

	@Data
	@Builder
	public static class RowContext {
		private final Long id;
		private final Property<String> name;
		private final Property<String> desc;
	}

	public static class RolePresenter implements IComponentPresenter<NamedObjectManagerContainer> {

		@Override
		public NamedObjectManagerContainer get() {
			UserRoleRepository repo = AppUI.getCurrent().getContext().getBean(UserRoleRepository.class);
			List<RowContext> rc = repo.findAll().stream()
					.map(r -> RowContext.builder()
							.desc(new ObjectProperty<String>(r.getDescription()))
							.id(r.getId())
							.name(new ObjectProperty<String>(r.getName()))
							.build())
					.collect(Collectors.toList());
			NamedObjectManagerContainer cont = new NamedObjectManagerContainer(rc, "Role");
			cont.onDelete(row -> {
				if (row.getId() != null) {
					repo.delete(row.getId());
				}
				AppUI.refresh();
			});
			cont.onSave(row -> {
				UserRole role = row.getId() != null
						? repo.findOne(row.getId())
						: UserRole.builder().build();
				role.setDescription(row.getDesc().getValue());
				role.setName(row.getName().getValue());
				repo.save(role);
				if (row.getId() == null) {
					AppUI.refresh();
				} else {
					ENotification.success("Saved " + role.getName());
				}
			});
			return cont;
		}
	}

	public static class PermissionDefinitionPresenter implements IComponentPresenter<NamedObjectManagerContainer> {

		@Override
		public NamedObjectManagerContainer get() {
			PermissionDefinitionRepository repo = AppUI.getCurrent().getContext().getBean(PermissionDefinitionRepository.class);
			List<RowContext> rc = repo.findAll().stream()
					.map(r -> RowContext.builder()
							.desc(new ObjectProperty<String>(r.getDescription()))
							.id(r.getId())
							.name(new ObjectProperty<String>(r.getName()))
							.build())
					.collect(Collectors.toList());
			NamedObjectManagerContainer cont = new NamedObjectManagerContainer(rc, "permission definition");
			cont.onDelete(row -> {
				if (row.getId() != null) {
					repo.delete(row.getId());
				}
				AppUI.refresh();
			});
			cont.onSave(row -> {
				PermissionDefinition def = row.getId() != null
						? repo.findOne(row.getId())
						: PermissionDefinition.builder().build();
				def.setDescription(row.getDesc().getValue());
				def.setName(row.getName().getValue());
				repo.save(def);
				if (row.getId() == null) {
					AppUI.refresh();
				} else {
					ENotification.success("Saved " + def.getName());
				}
			});
			return cont;
		}
	}
}
