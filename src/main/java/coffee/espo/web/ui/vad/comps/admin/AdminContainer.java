package coffee.espo.web.ui.vad.comps.admin;

import com.vaadin.ui.Button;

import coffee.espo.web.core.ui.vad.comps.EColumns;
import coffee.espo.web.core.ui.vad.comps.EComponent;
import coffee.espo.web.core.ui.vad.comps.ERows;
import coffee.espo.web.ui.vad.AppUI;
import coffee.espo.web.ui.vad.views.AdminView;

public class AdminContainer extends EColumns {

	private static final long serialVersionUID = 5852074474414822542L;

	public AdminContainer(EComponent content) {
		super(true);
		setWidthFull();
		
		ERows subNav = new ERows(
				getNavButton("Roles", AdminView.linkRoles()),
				getNavButton("Permission Definitions", AdminView.linkPermissionDefinitions()),
				getNavButton("Permission Groups", AdminView.linkPermissionGroups())
				);
		subNav.setWidth(205, Unit.PIXELS);
		addComponent(subNav);
				
		addComponent(content);
		content.setWidthFull();
		
		setExpandRatio(content, 3f);
	}
	
	protected Button getNavButton(String text, String navPath) {
		Button b = new Button(text, e -> AppUI.navigateTo(navPath));
		b.setWidth(100, Unit.PERCENTAGE);
		return b;
	}
}
