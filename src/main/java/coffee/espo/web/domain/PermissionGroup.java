package coffee.espo.web.domain;

import static javax.persistence.CascadeType.REMOVE;
import static org.hibernate.annotations.OnDeleteAction.CASCADE;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="perm_groups")
public class PermissionGroup {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="domain")
	private String domain;
	
	@OnDelete(action=CASCADE)
	@ManyToOne(cascade=REMOVE)
	@JoinColumn(name="role_id")
	private UserRole role;
	
	@OnDelete(action=CASCADE)
	@OneToMany(cascade=REMOVE, mappedBy="group", fetch=FetchType.EAGER)
	private List<Permission> permissions;
}
