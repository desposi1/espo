package coffee.espo.web.domain;

import static javax.persistence.CascadeType.REMOVE;
import static org.hibernate.annotations.OnDeleteAction.CASCADE;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;

import coffee.espo.web.api.PermissionValueType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="perms")
public class Permission {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@OnDelete(action=CASCADE)
	@ManyToOne(cascade=REMOVE)
	@JoinColumn(name="def_id")
	private PermissionDefinition definition;
	
	@Column(name="value")
	@Enumerated(EnumType.STRING)
	private PermissionValueType value;
	
	@OnDelete(action=CASCADE)
	@ManyToOne(cascade=REMOVE)
	@JoinColumn(name="group_id")
	private PermissionGroup group;
}
